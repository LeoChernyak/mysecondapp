//
//  Wage.swift
//  WindowShopper
//
//  Created by LeoChernyak on 15/12/2018.
//  Copyright © 2018 LeoChernyak. All rights reserved.
//

import Foundation
class Wage {
    
    class func getHours (forWage wage: Double, andPrice price: Double) -> Int{
        
    return Int (ceil(price / wage))
    }
    
}
