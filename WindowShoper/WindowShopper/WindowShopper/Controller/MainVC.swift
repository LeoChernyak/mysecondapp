//
//  ViewController.swift
//  WindowShopper
//
//  Created by LeoChernyak on 14/12/2018.
//  Copyright © 2018 LeoChernyak. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    @IBOutlet weak var WageTxt: CustomTxtField!
    @IBOutlet weak var priceTxt: CustomTxtField!
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let calcBtn = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 60))
        calcBtn.backgroundColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        calcBtn.setTitle("Calculate", for:.normal)
        calcBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for:.normal)
        calcBtn.addTarget(self, action: #selector(MainVC.calculate), for: .touchUpInside)
        WageTxt.inputAccessoryView = calcBtn
        priceTxt.inputAccessoryView = calcBtn
        
        resultLabel.isHidden = true
        hoursLabel.isHidden = true
        
        
    }
    
    @objc func calculate() {
        if let wageTxt = WageTxt.text, let priceText = priceTxt.text {
            if let wage = Double(wageTxt), let price = Double(priceText){
            view.endEditing(true)
            resultLabel.isHidden = false
            hoursLabel.isHidden = false
            resultLabel.text = "\(Wage.getHours(forWage: wage, andPrice: price))"
        }
    }
}
    
    
    @IBAction func ClearCalcBtn(_ sender: Any) {
        resultLabel.isHidden = true
        hoursLabel.isHidden = true
        WageTxt.text = " "
        priceTxt.text = " "
        
    }

}

