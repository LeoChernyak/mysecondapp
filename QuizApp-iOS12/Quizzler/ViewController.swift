//
//  ViewController.swift
//  Quizzler
//
//  Created by Angela Yu on 25/08/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Place your instance variables here
    let questionBank = QuestionBank()
    var pickedAnswer : Bool = false
    var questionNumber : Int = 0
    var score : Int = 0
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var progressBar: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let firstQuestion = questionBank.list[questionNumber]
//        questionLabel.text = firstQuestion.questionText
        nextQuestion()
        
        
        
    }

//tied up with 2 buttons (true and false) (tags)
    @IBAction func answerPressed(_ sender: AnyObject) {
        if sender.tag == 1 {
            pickedAnswer = true
        }
        else if sender.tag == 2 {
            pickedAnswer = false
        }
        
        checkAnswer()
        questionNumber += 1
        nextQuestion()
       
        
    }
    
    
    func updateUI() {
        
        scoreLabel.text = "Score: \(score)"
        progressLabel.text = "\(questionNumber + 1) / 15"
        
        
        // achanging of progress bar
        progressBar.frame.size.width = (view.frame.width / 14) * CGFloat(questionNumber)

        
        
        
      
    }
    

    func nextQuestion() {
        
       
        
        if questionNumber <= 14 {
            questionLabel.text = questionBank.list[questionNumber].questionText
            
             updateUI()
        } else {
            // alerts presenting
            let alert = UIAlertController(title: "U`ve done this", message: "Do u wanna restart queze?", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Restart", style: .default) { (UIAlertAction) in
                self.startOver()
            }
            
            alert.addAction(alertAction)
            
            present(alert, animated: true, completion: nil)
            
            
        }
    }
    
    
    func checkAnswer() {
        
        let correctAnswer = questionBank.list[questionNumber].questionAnswer
        
        if pickedAnswer == correctAnswer{
          
            ProgressHUD.showSuccess("Awesome! It`s correct answer")
            
            score = score + 1
            
        } else {
            
            ProgressHUD.showError("Booooo!!! Wrong")
            
        }
    }
    
    
    func startOver() {
       
        questionNumber = 0
        score = 0
    
        
        nextQuestion()
    }
    

    
}
