//
//  ProductCell.swift
//  CoderSwagShop
//
//  Created by LeoChernyak on 19/12/2018.
//  Copyright © 2018 LeoChernyak. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productLabelDescription: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    func updateViews(product: Product){
        productImage.image = UIImage(named: product.imageName)
        productPrice.text = product.price
        productLabelDescription.text = product.title
    }
}
