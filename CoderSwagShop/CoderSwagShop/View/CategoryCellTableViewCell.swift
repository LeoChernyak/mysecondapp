//
//  CategoryCellTableViewCell.swift
//  CoderSwagShop
//
//  Created by LeoChernyak on 18/12/2018.
//  Copyright © 2018 LeoChernyak. All rights reserved.
//

import UIKit

class CategoryCellTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTtitle: UILabel!

    func updateViews(category: Category){
        categoryImage.image = UIImage(named: category.imageName)
        categoryTtitle.text = category.title
    }
}
