//
//  DataService.swift
//  CoderSwagShop
//
//  Created by LeoChernyak on 19/12/2018.
//  Copyright © 2018 LeoChernyak. All rights reserved.
//

import Foundation
class DataService {
    static let instance = DataService()
//  Одна копия даты  для памяти Штука для прогрузки историй для face-six и карточек
    
    private let categories = [
        Category(title: "GUNS", imageName: "Guns"),
        Category(title: "ВОЕННИКИ", imageName: "Sasha"),
        Category(title: "PASSPORTS", imageName: "Passports"),
        Category(title: "COCAIN", imageName: "Cocain"),
        Category(title: "MARIJUANA", imageName: "images"),
        Category(title: "LSD", imageName: "lsd"),
        Category(title: "SLUTS", imageName: "Sluts")
    ]
    
    
    private let itemsOfguns = [
        Product(title: "Smith&Watson by Tarantino Movie", price: "Price 50$", imageName: "gun1"),
        Product(title: "AK47 by Russian production", price: "Price 150$", imageName: "gun2"),
        Product(title: "Colt 554653453453", price: "Price 30$", imageName: "gun3"),
        Product(title: "Glock downStair Jackson", price: "Price 100$", imageName: "gun4"),
        Product(title: "Desert Eagle by Israel Army Force", price: "Price 400$", imageName: "gun5"),
        Product(title: "Bro Gunner Dier", price: "Price 150$", imageName: "gun6")
        
    ]
    
    private let itemsOfWar = [
        Product(title: "Военник - Хуесос", price: "50000 рублей", imageName: "hui"),
        Product(title: "Военник - Хуесос", price: "50000 рублей", imageName: "kir"),
        Product(title: "Военник - Хуесос", price: "50000 рублей", imageName: "leo"),
        Product(title: "Военник - Хуесос", price: "50000 рублей", imageName: "yan"),
        Product(title: "Военник - Хуесос", price: "50000 рублей", imageName: "yasha"),
        Product(title: "Военник - Хуесос", price: "50000 рублей", imageName: "Sasha"),
        Product(title: "Военник - Хуесос", price: "50000 рублей", imageName: "nik")
    ]
    
    private let itemsOfSluts = [
        Product(title: "Аня", price: "50000 рублей", imageName: "slut1"),
        Product(title: "Ира", price: "50000 рублей", imageName: "slut2"),
        Product(title: "Лена", price: "50000 рублей", imageName: "slut3"),
        Product(title: "Катя", price: "50000 рублей", imageName: "slut4"),
        Product(title: "Арина", price: "50000 рублей", imageName: "slut5"),
        Product(title: "Жанна", price: "50000 рублей", imageName: "slut6"),
        Product(title: "Никита", price: "50000 рублей", imageName: "nik")
    ]
    
    private let itemsOfPassports = [Product]()
    
    
    
    func getCategories() -> [Category] {
        return categories
    }
    
    
    func getProducts(forCategoryTitle title:String) -> [Product] {
        switch title {
        case "GUNS":
           return getGuns()
        case "ВОЕННИКИ":
           return getWar()
        case "SLUTS":
           return getSluts()
        case "PASSPORTS":
           return getPassports()
        default:
            return getSluts()
        }
    }
    
    func getGuns() -> [Product] {
        
        return itemsOfguns
        
    }
    
    func getSluts() -> [Product] {
        return itemsOfSluts
    }
    
    func getWar() -> [Product] {
        return itemsOfWar
    }
    
    func getPassports() -> [Product]{
        return itemsOfPassports
    }
}
