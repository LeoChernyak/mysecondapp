//
//  Constants.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 07/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import Foundation

//for api
typealias CompletionHandler = (_ Success: Bool) -> ()


//URL CONSTANTS

let BASE_URL = "https://mozenrotapichat.herokuapp.com/v1/"
let URL_REGISTER = "\(BASE_URL)account/register"
let URL_LOGIN = "\(BASE_URL)account/login"
let URL_ADDUSER = "\(BASE_URL)user/add"
let URL_USER_BY_EMAIL = "\(BASE_URL)user/byEmail/" // For face six

let URL_GET_CHANNELS = "\(BASE_URL)channel"
let URL_GET_MESSAGES = "\(BASE_URL)message/byChannel"


let BAREAR_HEADER = [
    "Authorization":"Bearer \(AuthService.instance.authToken)",
    "Content-Type": "application/json; charset=utf-8"
]


//Seques
let TO_LOGIN = "toLogin"
let TO_CREATEACCOUNT = "createAnAccount"
let TO_UNWINDSEGUE = "unwindToChannel"
let TO_CHOOSE_AVATAR = "chooseAvatar"


//Colors
let COLOR_FONT_TEXT = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)


//Notifications Constants

let NOTIF_USER_DATA_DID_CHANGE = Notification.Name("notifUserDataChanged")
let NOTIF_CHANNELS_LOADED = Notification.Name("channelsLoaded")
let NOTIF_CHANNELS_SELECTED = Notification.Name("channelSelected")
let NOTIF_MESSAGES_DOWNLOADED = Notification.Name("messagesDownloaded")

//USER DEFAULTS
let TOKKEN_KEY = "token"
let LOGGED_IN_KEY = "loggedIn"
let USER_EMAIL = "userEmail"


//HEADERS
let HEADER = [
    "Content-Type": "application/json; charset=utf-8"
]
