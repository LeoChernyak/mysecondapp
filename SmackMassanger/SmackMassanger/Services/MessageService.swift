//
//  MessageService.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 15/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MessageService {
    static let instance = MessageService() //инициализация класса
    
    var channels = [Channel]() // объекты структуры model channel
    var selectedChannel : Channel?
    
    //массив сообщений для загрузки всех сообщений
    var messages = [Message]()
    
    //JSON PARSING
    func findAllChannel(completion: @escaping CompletionHandler) {
        Alamofire.request(URL_GET_CHANNELS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BAREAR_HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                //new way parsing SWIFT 4
//                do {
//                   self.channels = try JSONDecoder().decode([Channel].self, from: data)
//                } catch let error {
//                    debugPrint(error as Any)
//                }
//                print(self.channels)
                
                //Old Way (Decodable SWIFT 4)
                if let json = JSON(data: data).array{
                    for item in json {// распаковка JSON
                        let name = item["name"].stringValue
                        let channelDescription = item["descriprion"].stringValue
                        let id = item["_id"].stringValue
                        let channel = Channel(ChannelTitle: name, ChannelDescription: channelDescription, id: id)// создание объекта
                        self.channels.append(channel)
                    }
                    NotificationCenter.default.post(name: NOTIF_CHANNELS_LOADED, object: nil)
                    completion(true)
                    
                }
                
                
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
        
    }
    //функция для получения всех сообщений (API запрос)
    func getAllMessages(channelId: String,completion: @escaping CompletionHandler){
        Alamofire.request("\(URL_GET_MESSAGES)\(channelId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BAREAR_HEADER).responseJSON { (response) in
            if response.result.error == nil{
                self.clearMessages()
                guard let data = response.data else {return}
                if let json = JSON(data: data).array{
                    for item in json {
                        let messageBody = item["messageBody"].stringValue
                        let channelId = item["channelId"].stringValue
                        let id = item["_id"].stringValue
                        let userName = item["userName"].stringValue
                        let userAvatar = item["userAvatar"].stringValue
                        let userAvatarColor = item["userAvatarColor"].stringValue
                        let timeStamp = item["timeStamp"].stringValue
                        
                        let message = Message(message: messageBody, userName: userName, channelId: channelId, userAvatar: userAvatar, userAvatarColor: userAvatarColor, id: id, timeStamp: timeStamp)
                        self.messages.append(message)
                        
                    }
                    NotificationCenter.default.post(name: NOTIF_MESSAGES_DOWNLOADED, object: nil)
                    completion(true)
                }
                
            } else {
                debugPrint(response.result.error as Any)
                completion(false)
            }
        }
    }
    
    
    
    //Функция очитски каналов, после LogOut
    func clearChannels() {
        channels.removeAll()
    }
    
    func clearMessages() {
        messages.removeAll()
    }
    
    
    
}
