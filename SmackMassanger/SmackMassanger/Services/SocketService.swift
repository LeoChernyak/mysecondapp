//
//  SocketService.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 29/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit
import SocketIO

class SocketService: NSObject {
    
    static let instance = SocketService()
    
    //Обяхательная функция
    override init() {
        super.init()
    }
    
    var socket : SocketIOClient = SocketIOClient(socketURL: URL(string: BASE_URL)!)
    
    //Открытие канала
    func establishConnection() {
        socket.connect()
    }
    
    // закрытие канала
    func closeConnection() {
        socket.disconnect()
    }
    
    //socket emit для отправки!!!
    func addChannel(channelName: String, channelDescription: String, completion: @escaping CompletionHandler) {
        socket.emit("newChannel", channelName, channelDescription)
        completion(true)
    }
    
    //socket dotOn для принятие ответа с webSocketApi нужна api документация
    func getChannel(completion: @escaping CompletionHandler) {
        socket.on("channelCreated") { (dataArray, ack) in
            guard let channelName = dataArray[0] as? String else {return}
            guard let channelDescription = dataArray[1] as? String else {return}
            guard let channelId = dataArray[2] as? String else {return}
            
            let newChannel = Channel(ChannelTitle: channelName, ChannelDescription: channelDescription, id: channelId)
            
            MessageService.instance.channels.append(newChannel)
            completion(true)
        }
    }
    
    func addMessage(messageBody: String, userId: String, channelId: String, completion: @escaping CompletionHandler) {
        let user = UserDataService.instance
        //Обращение через объект
        socket.emit("newMessage", messageBody, userId, channelId, user.name, user.avatarName, user.avatarColor)
        completion(true)
    }
    
    
    func getChatMessage(completion: @escaping CompletionHandler) {
        socket.on("messageCreated") { (dataArray, ack) in
            guard let msgBody = dataArray[0] as? String else {return}
            guard let chanelId = dataArray[2] as? String else {return}
            guard let userName = dataArray[3] as? String else {return}
            guard let userAvatar = dataArray[4] as? String else {return}
            guard let userAvatarColor = dataArray[5] as? String else {return}
            guard let msgId = dataArray[6] as? String else {return}
            guard let timeStamp = dataArray[7] as? String else {return}
            
            if chanelId == MessageService.instance.selectedChannel?.id && AuthService.instance.islogIn {
                let newMessage = Message(message: msgBody, userName: userName, channelId: chanelId, userAvatar: userAvatar, userAvatarColor: userAvatarColor, id: msgId, timeStamp: timeStamp)
                
                MessageService.instance.messages.append(newMessage)
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func getTypingUsers (_ completionHandeler: @escaping (_ typingUsers: [String: String]) -> Void) {
        socket.on("userTypingUpdate") { (dataArray, ack) in
            guard let typingUsers = dataArray [0] as? [String:String] else {return}
            completionHandeler(typingUsers)
        }
    }
    

}
