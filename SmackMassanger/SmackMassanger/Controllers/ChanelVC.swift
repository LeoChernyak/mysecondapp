//
//  ChanelVC.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 06/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit

class ChanelVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
 
    
    
    //Outlets
    @IBOutlet weak var userImg: CircleImage!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    @IBAction func prepareOnUnwind(segue: UIStoryboardSegue){} // возвратиться на этот экран
    
    @IBAction func addChannelBtnPressed(_ sender: Any) {
        if AuthService.instance.islogIn == true {
        let addChannel = AddChannelVC()
        addChannel.modalPresentationStyle = .custom
            present(addChannel, animated: true, completion: nil)
        }
    }
    
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
        // initDefaultProperties = изменение настроек выплываюшеего VC
        NotificationCenter.default.addObserver(self, selector: #selector(ChanelVC.userDataDidChanged(_:)), name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        //прогрузка каналов после входа
        NotificationCenter.default.addObserver(self, selector: #selector(ChanelVC.channelsLoaded(_:)), name: NOTIF_CHANNELS_LOADED, object: nil)
        
        
        //Используем сокет, там где нам необходимо видеть изменения
        
        SocketService.instance.getChannel { (success) in
            if success {
                self.tableView.reloadData() //обновление тэйблвьюхи
            }
        }
    }
    
    
    //проверка залогинился ты или нет при въоде в приложение
    override func viewDidAppear(_ animated: Bool) {
        setUserInfo()
    }
    
    //функция для notification center и обновления тайтла логина и аватара
    @objc func userDataDidChanged(_ notif: Notification) {
        setUserInfo()
        
        
    }
    //прогрузка каналов после входа
    @objc func channelsLoaded(_ notid: Notification){
        tableView.reloadData()
    }
    
    
    func setUserInfo() {
        if AuthService.instance.islogIn {
            loginBtn.setTitle(UserDataService.instance.name, for: .normal)
            userImg.image = UIImage(named: UserDataService.instance.avatarName)
            userImg.backgroundColor = UserDataService.instance.returnUIColor(components: UserDataService.instance.avatarColor)
        } else {
            loginBtn.setTitle("Login", for: .normal)
            userImg.image = UIImage(named: "menuProfileIcon")
            userImg.backgroundColor = UIColor.clear
            tableView.reloadData()//для чистки каналов после логаута
        }
        
        
    }
    
    
//    //Обновление Тайтла Логина после регистрации (отсебятина)
//    override func viewDidAppear(_ animated: Bool) {
////        if UserDataService.instance.name != "" {
////            loginBtn.titleLabel?.text = UserDataService.instance.name
////        }
//    }
    // представление ProfileVC Xciba
    @IBAction func LoginBtnPressed(_ sender: Any) {
        if AuthService.instance.islogIn {
            //show profile page
            let profile = ProfileVC()
            profile.modalPresentationStyle = .custom
            present(profile, animated: true, completion: nil)
            
        } else {
        performSegue(withIdentifier: TO_LOGIN, sender: nil)
        }
    }
    
    
    
    // Настройка TableView определение количества строк и столбцов
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "channelCell", for: indexPath) as? ChannelCell {
            let channel = MessageService.instance.channels[indexPath.row]
            cell.configureCell(channel: channel)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.instance.channels.count
    }
    
    //функция выбирающая из таблицы значения
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = MessageService.instance.channels[indexPath.row]
        MessageService.instance.selectedChannel = channel
        NotificationCenter.default.post(name: NOTIF_CHANNELS_SELECTED, object: nil)
        //slideback после того как выбрал канал
        self.revealViewController().revealToggle(animated: true)
    }
    
}
