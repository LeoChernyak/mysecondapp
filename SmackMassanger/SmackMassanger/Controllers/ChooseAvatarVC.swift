//
//  ChooseAvatarVCViewController.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 10/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit



//добавить в имя класса делигаты

class ChooseAvatarVCViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    //Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    //Varibles
    //Переменная для смены Dark or Light segments
    var avatarType = AvatarType.dark
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Реализация Collection View делигаиов
        collectionView.delegate = self
        collectionView.dataSource = self
        

        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "avatarCell", for: indexPath) as? CellViewCollectionViewCell {
            //вызываем функцию их CellView для заполнения коллекции картинками
            cell.configureCell(index: indexPath.item, type: avatarType)
            return cell
        }
        return CellViewCollectionViewCell()
    }
    // количествос коллекций
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // количество фотографий
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 28
    }
    
    //выбрать айтэм из коллекции
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if avatarType == .dark {
            UserDataService.instance.setAvatarName(avatarName: "dark\(indexPath.item)")
        } else {
            UserDataService.instance.setAvatarName(avatarName: "light\(indexPath.item)")
        }
        self.dismiss(animated: true, completion: nil)
    }


    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
   
    @IBAction func changeSegments(_ sender: Any) {
        if segmentControl.selectedSegmentIndex == 0 {
            avatarType = AvatarType.dark
        } else {
            avatarType = AvatarType.light
        }
        collectionView.reloadData() // для моментальной перегрузки данных
    }
    
    
    
    
}
