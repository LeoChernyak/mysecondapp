//
//  CreatAccountVC.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 07/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit

class CreatAccountVC: UIViewController {
    
    //Outlets
    
    @IBOutlet weak var usernameTxt: UITextField!
    
    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var passwordTxt: UITextField!
    
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    //Variebles for test
    
    var avatarName = "Profile icon"
    var avatarColor = "[0.5, 0.5, 0.5, 1]"
    var bgColor : UIColor?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // обязательна для setUpView
        setUpView()
        

        // Do any additional setup after loading the view.
    }
    //функция для обновления View Controller`a и изменения картинки
    override func viewDidAppear(_ animated: Bool) {
        if UserDataService.instance.avatarName != "" {
            
            userImg.image = UIImage(named: UserDataService.instance.avatarName)
            avatarName = UserDataService.instance.avatarName
            if avatarName.contains("light") && bgColor == nil {
                userImg.backgroundColor = UIColor.lightGray
            }
        }
    }
    
    //изменение цвета backgrounda
    @IBAction func generateBGPressed(_ sender: Any) {
       let r = CGFloat(arc4random_uniform(255)) / 255 // рандом RGB цвета
        let g = CGFloat(arc4random_uniform(255)) / 255
        let b = CGFloat(arc4random_uniform(255)) / 255
        bgColor = UIColor(red: r, green: g, blue: b, alpha: 1)
        avatarColor = "[\(r), \(g), \(b), 1]"
        
        //добавление анимации
        UIView.animate(withDuration: 2) {
            self.userImg.backgroundColor = self.bgColor
        }
        
       
        
    }
    
     //перейти к выбору аватара
    @IBAction func chooseAvatarPressed(_ sender: Any) {
         performSegue(withIdentifier: TO_CHOOSE_AVATAR, sender: nil)
    }
    
    @IBAction func createActionPressed(_ sender: Any) {
        spinner.isHidden = false
        spinner.startAnimating()
        
        guard let email = emailTxt.text , emailTxt.text != "" else {return}//не может быть пустой строкой
        guard let pass = passwordTxt.text, passwordTxt.text != "" else {return}
        guard let name = usernameTxt.text, usernameTxt.text != "" else {return}
        
        // Подстановка вводимых данных в URL запрос
        AuthService.instance.registerUser(email: email, password: pass) { (success) in
            if success {
//                print("registrated user!")
                AuthService.instance.loginUser(email: email, password: pass, completion: { (success) in
                    if success {
//                        print("You are logged in", AuthService.instance.authToken) // получение токена User`a
                        AuthService.instance.createUser(name: name, email: email, avatarName: self.avatarName, avatarColor: self.avatarColor, complition: { (success) in
                            if success {
                                 print("All data has been reservated")
                                self.spinner.isHidden = true
                                self.spinner.stopAnimating()
                                AuthService.instance.islogIn = true
                                self.performSegue(withIdentifier: TO_UNWINDSEGUE, sender: nil)
                                // переход на домашнюю страницу после регестрации
                                NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                                
                                
                                
                            }
                        
                        })
                    }
                })
            }
        }
        
    }
    
    //возвратиться на 2 экрана назад
    @IBAction func unwindToChannel(_ sender: Any) {
        performSegue(withIdentifier: TO_UNWINDSEGUE, sender: nil)
    }
   
    
    //Установка окончательного вида, если че то не устраивает
    
    func setUpView() {
        //прогрузка спинера
        spinner.isHidden = true
        
        // от себя
//        self.usernameTxt.textColor = COLOR_FONT_TEXT
//        self.emailTxt.textColor = COLOR_FONT_TEXT
//        self.passwordTxt.textColor = COLOR_FONT_TEXT
        self.usernameTxt.attributedPlaceholder = NSAttributedString(string: "username", attributes: [NSAttributedStringKey.foregroundColor: COLOR_FONT_TEXT])
        self.emailTxt.attributedPlaceholder = NSAttributedString(string: "email", attributes: [NSAttributedStringKey.foregroundColor: COLOR_FONT_TEXT])
        self.passwordTxt.attributedPlaceholder = NSAttributedString(string: "password", attributes: [NSAttributedStringKey.foregroundColor: COLOR_FONT_TEXT])
        
        
        //скрытие клавиатуры
        let tap = UITapGestureRecognizer(target: self, action: #selector(CreatAccountVC.handleTap))
        view.addGestureRecognizer(tap)
    }
    
    //функиця для скрытия клавиатуры
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    
    // обязательна для setUpView
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setUpView()
    }
    
   
    

}
