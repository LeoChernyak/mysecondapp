//
//  ChatVC.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 06/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit

class ChatVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    //OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuBtn: UIButton!
    
    @IBOutlet weak var typingUserLbl: UILabel!
    
    @IBOutlet weak var sendMsgBtn: UIButton!
    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var messageTxtBox: UITextField!
    
    
    
    //Variables
    
    var isTyping = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //функция для поднятия клавиатуры вместе с текстфилдом
        view.bindToKeyboard()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //показывать длинные сообщения полностью
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        sendMsgBtn.isHidden = true 
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ChatVC.handleTap))
        view.addGestureRecognizer(tap)
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside) // выскакиваюзее меню по кнопке
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())//*
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())// меню драгингом
       
        
        //странная тема, что такое Observer
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.userDataDidChanged(_:)), name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.channelSelected(_:)), name: NOTIF_CHANNELS_SELECTED, object: nil)
        
        // сокет для обновления эррэя сообщений в реальном времени
        SocketService.instance.getChatMessage { (success) in
            if success {
                self.tableView.reloadData()
                //skroll down to the bottom if you have a new message
                if MessageService.instance.messages.count > 0 {
                    let indexPath = IndexPath(row: MessageService.instance.messages.count-1, section: 0)
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
        
        
        //сокет для тайпинга
        
        SocketService.instance.getTypingUsers { (typingUsers) in
            guard let channelId = MessageService.instance.selectedChannel?.id else {return}
            var names = ""
            var numerOfTypers = 0
            
            for (typingUser,chanel) in typingUsers {
                if typingUser != UserDataService.instance.name && chanel == channelId {
                    if names == "" {
                        names = typingUser
                    } else {
                        names = "\(names), \(typingUser)"
                    }
                    numerOfTypers += 1
                }
            }
            if numerOfTypers > 0 && AuthService.instance.islogIn == true {
                var verb = "is"
                if numerOfTypers > 1 {
                    verb = "are"
                }
                self.typingUserLbl.text = "\(names) \(verb) typing a message"
            } else {
                self.typingUserLbl.text = ""
            }
        }
        
        //Проверка логинга, в случае закрытия приложения
        if AuthService.instance.islogIn {
            AuthService.instance.findUserByEmail { (success) in
                NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
            }
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }
    @objc func userDataDidChanged(_ notif: Notification) {
        if AuthService.instance.islogIn == true {
        // get cchanels
            onLogInGetMessages()
        } else {
            mainLabel.text = "Please Log in"
            tableView.reloadData()
        }
    }
    
    
    @objc func channelSelected(_ notif: Notification){
        updateWithChannel()
    }
    
    //для тапа клавы
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    //изменение заголовка при выборе чата
    func updateWithChannel() {
        let channelName = MessageService.instance.selectedChannel?.ChannelTitle ?? ""
        mainLabel.text = "#\(channelName)"
        getMessages()
    }
    
    //func для доступности к кнопке send при редактирование
    
    @IBAction func messageTextBoxEditing(_ sender: Any) {
        guard let channelId = MessageService.instance.selectedChannel?.id else {return}
        
        if messageTxtBox.text == "" {
            isTyping = false
            sendMsgBtn.isHidden = true
            
            //сокет для тайпинга
            SocketService.instance.socket.emit("stopType", UserDataService.instance.name, channelId)
        } else {
            if isTyping == false {
                sendMsgBtn.isHidden = false
                SocketService.instance.socket.emit("startType", UserDataService.instance.name, channelId)
            }
            isTyping = true
        }
    }
    
    
    
    
    @IBAction func sendMessageBtn(_ sender: Any) {
        if AuthService.instance.islogIn {
            //отправка сообщения
            guard let channelId = MessageService.instance.selectedChannel?.id else {return}
            guard let message = messageTxtBox.text else {return}
            
            SocketService.instance.addMessage(messageBody: message, userId: UserDataService.instance.id, channelId: channelId) { (success) in
                if success {
                    //отчистка после отправки
                    self.messageTxtBox.text = ""
                    self.messageTxtBox.resignFirstResponder()
                     SocketService.instance.socket.emit("stopType", UserDataService.instance.name, channelId)
                }
            }
            
        }
    }
    
    func onLogInGetMessages() {
        MessageService.instance.findAllChannel { (success) in
            if success {
                //do staff with channells при входе будет выбираться первый чат по дефолту
                if MessageService.instance.channels.count > 0 {
                    MessageService.instance.selectedChannel = MessageService.instance.channels[0]
                    self.updateWithChannel()
                } else {
                    self.mainLabel.text = "No Channels yet"
                }
            }
        }
    }
    
    //функция получения сообщений
    func getMessages(){
        //unwraping channel id
        guard let channelId = MessageService.instance.selectedChannel?.id else {return}
        MessageService.instance.getAllMessages(channelId: channelId) { (success) in
            if success {
                self.tableView.reloadData()
            }
        }
        
        
        
        
        
        
    }
    
    //для показухи сообзения
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as? MessageCell {
            let message = MessageService.instance.messages[indexPath.row]
            cell.configCell(message: message)
            return cell
        } else {
            return UITableViewCell()
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.instance.messages.count
    }
    
    
}
