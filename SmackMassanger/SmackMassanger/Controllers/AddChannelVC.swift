//
//  AddChannelVC.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 29/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit

class AddChannelVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var channelNameTxt: UITextField!
    @IBOutlet weak var descriptionChennelTxt: UITextField!
    
    @IBOutlet weak var bgView: UIView!
    
    
    //Actions
    @IBAction func closeBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addChannelBtnPressed(_ sender: Any) {
        guard let channelName = channelNameTxt.text , channelNameTxt.text != "" else {return}
        guard let channelDescription = descriptionChennelTxt.text , descriptionChennelTxt.text != "" else {return}
        SocketService.instance.addChannel(channelName: channelName, channelDescription: channelDescription) { (success) in
            if success {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()

       
    }
    
    func setUpView() {
        
        
        //Дизайн текста через NSA 
        channelNameTxt.attributedPlaceholder = NSAttributedString(string: "Channel Name", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)])
        
        descriptionChennelTxt.attributedPlaceholder = NSAttributedString(string: "Description", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)])
        
        let closeTouch = UITapGestureRecognizer(target: self, action: #selector(AddChannelVC.closeVCTap) )
        bgView.addGestureRecognizer(closeTouch)
    }
    
    @objc func closeVCTap(_ recognizer: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }

   
}
