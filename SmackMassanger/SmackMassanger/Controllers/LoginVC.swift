//
//  LoginVC.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 07/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var userNameTxt: UITextField!
    
    @IBOutlet weak var passwordTxt: UITextField!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()

        
    }
    
    //Actions
    
    @IBAction func backToPreviousePage(_ sender: Any) {
    dismiss(animated: true, completion: nil)
    }
    //вернуться на предыдущий экран
    
    
    @IBAction func createAccountBtn(_ sender: Any) {
        performSegue(withIdentifier: TO_CREATEACCOUNT , sender: nil)
    }
    
    
    @IBAction func pressedLoginBtn(_ sender: Any) {
        spinner.isHidden = false
        spinner.startAnimating()
        guard let email = userNameTxt.text, userNameTxt.text != "" else {return}
        guard let password = passwordTxt.text, passwordTxt.text != "" else {return}
        
        // URL ЗАПРОС get - face-six
        AuthService.instance.loginUser(email: email, password: password) { (success) in
            if success {
                AuthService.instance.findUserByEmail(completion: { (success) in
                    if success {
                         AuthService.instance.islogIn = true
                        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                        self.spinner.isHidden = true
                        self.spinner.stopAnimating()
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }
        }
        
        
    }
    
    
    //Other Functions
    func setUpView() {
        self.spinner.isHidden = true
        self.userNameTxt.attributedPlaceholder = NSAttributedString(string: "username", attributes: [NSAttributedStringKey.foregroundColor: COLOR_FONT_TEXT])
        self.passwordTxt.attributedPlaceholder = NSAttributedString(string: "password", attributes: [NSAttributedStringKey.foregroundColor: COLOR_FONT_TEXT])
        
    }
    
    
}
