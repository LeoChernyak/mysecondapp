//
//  ProfileVC.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 14/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userEmail: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    
    
    //Actions
    
    @IBAction func backToPrevious(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func logOutPressed(_ sender: Any) {
        UserDataService.instance.logOutUser()
        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()

        // Do any additional setup after loading the view.
    }

   
    
    
    func setupView() {
        self.userName.text = UserDataService.instance.name
        self.userEmail.text = UserDataService.instance.email
        self.userImg.image = UIImage(named: UserDataService.instance.avatarName)
        self.userImg.backgroundColor = UserDataService.instance.returnUIColor(components: UserDataService.instance.avatarColor)
        
        let closeTouch = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.closeVCTap) )
        bgView.addGestureRecognizer(closeTouch)
    }
    
    
    @objc func closeVCTap(_ recognizer: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }

 

}
