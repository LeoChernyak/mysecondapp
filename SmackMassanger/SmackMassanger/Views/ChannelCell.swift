//
//  ChannelCell.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 29/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {
    //Outlets
    
    @IBOutlet weak var channelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.layer.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        } else {
            self.layer.backgroundColor = UIColor.clear.cgColor
            
        }
        // Configure the view for the selected state
    }
    
    func configureCell(channel : Channel) {
        let title = channel.ChannelTitle ?? ""
        channelName.text = title
    }

}
