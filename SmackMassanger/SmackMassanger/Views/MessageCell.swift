//
//  MessageCell.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 06/03/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    
    //Outlets
    
    @IBOutlet weak var messageBodyLbl: UILabel!
    @IBOutlet weak var userImg: CircleImage!
    
    @IBOutlet weak var timeStampLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(message: Message) {
        messageBodyLbl.text = message.message
        userNameLbl.text = message.userName
        userImg.image = UIImage(named: message.userAvatar)
        userImg.backgroundColor = UserDataService.instance.returnUIColor(components: message.userAvatarColor)
    }

}
