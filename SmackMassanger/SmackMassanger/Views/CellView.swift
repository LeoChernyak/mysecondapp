//
//  CellViewCollectionViewCell.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 10/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit



enum AvatarType {
    case dark
    case light
}


class CellViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var avatarImg: UIImageView!
    
    //появление вьюхи
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpView()
        
    }
    
    
    //Логика переключения между Statements Controller
    func configureCell(index: Int, type: AvatarType) {
        if type == AvatarType.dark {
            avatarImg.image = UIImage(named: "dark\(index)")
            self.layer.backgroundColor = UIColor.darkGray.cgColor
        } else {
            avatarImg.image = UIImage(named: "light\(index)")
            self.layer.backgroundColor = UIColor.gray.cgColor
        }
        
    }
    

    //настройки img
    func setUpView() {
        self.layer.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        
        
        
    }
}
