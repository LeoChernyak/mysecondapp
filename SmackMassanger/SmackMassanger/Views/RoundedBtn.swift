//
//  RoundedBtn.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 08/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedBtn: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 6.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            
        }
    }
    
    override func awakeFromNib() {
        self.setUpView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setUpView()
    }
    
    func setUpView() {
        self.layer.cornerRadius = cornerRadius
        
    }

}
