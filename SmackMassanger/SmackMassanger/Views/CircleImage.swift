//
//  CircleImage.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 10/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import UIKit

@IBDesignable
class CircleImage: UIImageView {

  //обязательная функция
    override func awakeFromNib() {
        setupView()
    }
    //получение круга
    func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
    
    //обязательная функция
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupView()
    }

}
