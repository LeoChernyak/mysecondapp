//
//  Channel.swift
//  SmackMassanger
//
//  Created by LeoChernyak on 15/01/2019.
//  Copyright © 2019 LeoChernyak. All rights reserved.
//

import Foundation

struct Channel: Decodable {
    //Old Way
    public private(set) var ChannelTitle: String!
    public private(set) var ChannelDescription: String!
    public private(set) var id: String!
    
    //Названия обязательны, такие же как и в JSON файле
//    public private(set) var _id: String!
//    public private(set) var name: String!
//    public private(set) var description: String!
//    public private(set) var __v: Int?
    
}
