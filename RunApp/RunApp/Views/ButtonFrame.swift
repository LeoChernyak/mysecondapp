//
//  ButtonFrame.swift
//  RunApp
//
//  Created by LeoChernyak on 10/12/2018.
//  Copyright © 2018 LeoChernyak. All rights reserved.
//

import UIKit

class ButtonFrame: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.white.cgColor
        
        
    }
}
