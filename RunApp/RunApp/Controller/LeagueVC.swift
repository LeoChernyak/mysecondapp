//
//  LeagueVC.swift
//  RunApp
//
//  Created by LeoChernyak on 10/12/2018.
//  Copyright © 2018 LeoChernyak. All rights reserved.
//

import UIKit

class LeagueVC: UIViewController {
    
    var player: Player!
    

    @IBOutlet weak var nextBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        player = Player();
    }
    
    
    
    @IBAction func onNextTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "skillVCSeque" , sender: self)
        
    }
    
    @IBAction func onMensTapped(_ sender: Any) {
        selectLeague(leagueType: "Man")
        
    }
    
    @IBAction func onWomensTapped(_ sender: Any) {
        selectLeague(leagueType: "Girl")
        
        
    }
    
    @IBAction func onCoachingTapped(_ sender: Any) {
        selectLeague(leagueType: "Coach")
    }
    
    
    func selectLeague (leagueType: String) {
        player.desiredLegue = leagueType
        nextBtn.isEnabled = true
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let skillVC = segue.destination as? SkillVC {
            skillVC.player = player
            
        }
    }
    
    
    
}
