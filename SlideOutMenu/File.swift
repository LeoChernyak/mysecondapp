//
//  File.swift
//  SlideOutMenu
//
//  Created by LeoChernyak on 17/12/2018.
//  Copyright © 2018 LeoChernyak. All rights reserved.
//

import Foundation
class BackTableVC: UITableViewController {
    
    var TableArray = [String]()
    override func viewDidLoad() {
        TableArray = ["Main","Camera","Settings"]
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: TableArray[indexPath.row], for: indexPath) as UITableViewCell
        cell.textLabel?.text = TableArray [indexPath.row]
        
        return cell
        //Создание пунктов меню
    }
    
    
  
}
