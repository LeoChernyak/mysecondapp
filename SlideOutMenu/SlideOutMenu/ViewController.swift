//
//  ViewController.swift
//  SlideOutMenu
//
//  Created by LeoChernyak on 17/12/2018.
//  Copyright © 2018 LeoChernyak. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var Label: UILabel!
    @IBOutlet weak var OpenMenu: UIBarButtonItem!
    
    var varView = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        OpenMenu.target = self.revealViewController()
        OpenMenu.action = Selector("revealToggle:")
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        //Перетягивание и открытие меню

        if (varView == 0){

            Label.text = "Strings"

        }
        else {
            Label.text = "OtherText"
        }
        
    }


}

