//
//  Main.swift
//  SlideOutMenu
//
//  Created by LeoChernyak on 17/12/2018.
//  Copyright © 2018 LeoChernyak. All rights reserved.
//

import Foundation

class Main : UIViewController {
    override func viewDidLoad() {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
}

